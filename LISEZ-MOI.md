Lisez-moi (s'il-vous-plaît)
===

## Markdown

### Retours à la ligne

Les retours à la ligne simples semblent ne pas provoquer de retour à la ligne lors conversion vers HTML (à confirmer).

### Listes à puce

Le nombre d'espaces à rajouter devant le caractère "\*" ou "-" pour faire une sous-liste est : 4.

## Notes de développement

* Vous pouvez définir COMPILE_LESS_INTO_CSS à False pour le développement. Notez cependant que les liens vers les ancres peuvent alors ne pas fonctionner.

## Ressources CSS utiles

* [Documentation de l'extension Markdown qui gère la génération de sommaires](https://python-markdown.github.io/extensions/toc/)
* [Flexbox : comprendre le dimensionnement des images](https://codepen.io/dudleystorey/pen/pejpYW)

