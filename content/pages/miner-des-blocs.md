Title: Forger des blocs

# Forger des blocs

## Miner des blocs ? Non : les forger !

Dans une monnaie libre, on ne peut pas "miner" des blocs.

Le terme "minage" renvoie en effet à la création monétaire qui est attribuée, dans certaines monnaies non libres, à ceux qui participent au calcul des blocs.

Ce mécanisme reproduit les systèmes monétaires basés sur les métaux précieux comme l'or.

Or, par définition, l'or ne peux pas être une monnaie libre (et ne peut d'ailleurs jamais rester très longtemps une monnaie tout court, du fait de sa rareté qui le rend facilement déflationniste).

## Pourquoi forger des blocs ?

Pour la même raison que vous partagez des fichiers torrents sans attendre de rémunération en retour : simplement
pour faire vivre le réseau. 

Il y a plusieurs facteurs à prendre en compte :

1. **Seuls les noeuds associés à un compte membre peuvent calculer les blocs.**
2. **Le calcul de bloc dans Duniter ne coûte presque rien.** Ce qui sécurise la blockchain est le fait que chaque calculateur
est explicitement associé à un être humain. Ainsi, pas de risque d'attaque Sybil, et il n'y a aucune course à la
puissance nécessaire ! Tant que votre PC est dans la moyenne des puissances existantes (environ du Raspberry Pi au PC 4 cœurs), votre nœud pourra participer au calcul des blocs.
3. **Vous ne devriez pas attendre que d'autres membres calculent pour vous.** Exécuter votre nœud, c'est vous assurer que le 
réseau respecte les règles communes inscrites dans la blockchain. Ce nœud s'assurera pour vous que les Dividendes
Universels sont correctements générés, et que les transactions sont transmises.

## Comment installer un nœud Duniter

### Installer son nœud Duniter

*   [Installer son nœud Duniter]({filename}miner-des-blocs/installer.md)
*   [Mettre à jour son nœud Duniter]({filename}miner-des-blocs/mettre-a-jour.md)
*   [Mettre à jour Duniter sur YunoHost en ligne de commande](https://forum.duniter.org/t/full-https-support-for-duniter-package-for-yunohost/1892/18)

### Configurer son nœud Duniter

* [Configurer son nœud Duniter fraichement installé]({filename}miner-des-blocs/configurer.md)
* [Avoir plusieurs pairs partageant une même clé]({filename}miner-des-blocs/cles-partagees.md)
* [Ajouter/Retirer des interfaces spécifiques de pair]({filename}miner-des-blocs/interfaces-specifiques-de-pair.md)

### Utiliser son nœud Duniter

*   [Lancer Duniter automatiquement au démarrage de la machine]({filename}miner-des-blocs/lancement-au-boot.md)
*   [Duniter en ligne de Commande]({filename}miner-des-blocs/commandes.md)
*   [Les modules (plugins)]({filename}miner-des-blocs/modules.md)
*   [Liste des modules (plugins)]({filename}miner-des-blocs/liste-modules.md)

### Auto-héberger son nœud Duniter

*   [Duniter sur un VPN](https://forum.duniter.org/t/duniter-sur-un-vpn/2280/13)
*   [Duniter sur YunoHost derrière une box privatrice (type livebox)](https://forum.duniter.org/t/duniter-sur-yunohost-derriere-une-box-privatrice-type-livebox/2169)
*   [Duniter sur Raspberry Pi 3 derrière une Freebox v5 avec Yunohost]({filename}miner-des-blocs/raspberry-pi-freebox-yunohost.md)

## Rémunération des forgerons

*   [Comment être rémunéré en calculant des blocs]({filename}miner-des-blocs/remuneration-calcul-blocs.md)


<section id="download">	
	<div>
		<h2>Télécharger Duniter</h2>

		<p>
			<a class="CTA" href="https://git.duniter.org/nodes/typescript/duniter/tags/2019.0407.1508">
				<span>Duniter 2019.0407.1508</span>
				<span>Réseau Ğ1-TEST (test)
			</a>

			<a class="CTA" href="https://git.duniter.org/nodes/typescript/duniter/tags/v1.7.21">
				<span>Duniter v1.7.21</span>
				<span>Réseau Ğ1 (réel)</span>
			</a>
		</figure>
	</div>
</section>
