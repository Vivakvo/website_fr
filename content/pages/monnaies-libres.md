Title: Une monnaie libre : une monnaie qui libère
Breadcrumb: Monnaie libre
BodyClass: pages-principales

# Particularités d'une monnaie libre

Duniter est un logiciel qui sert à propulser une monnaie libre.

Mais qu'est-ce qu'une monnaie libre ?

Les monnaies libres sont une notion introduite en 2010 par un mathématicien du nom de Stéphane Laborde, dans un ouvrage intitulé la **Théorie Relative de la Monnaie** (TRM).

Une monnaie libre présente plusieurs différences majeures avec les autres types de monnaies&nbsp;:

<section id="instrument-de-mesure">
	<div>
		<h2>
			La monnaie pensée comme un instrument de mesure
		</h2>

		<figure>

			<figcaption>
				<p>
					Dès l'Antiquité, Aristote a énoncé que la monnaie répondait à plusieurs fonctions, parmi lesquelles celle de <strong>mesurer les valeurs économiques échangées</strong>.
				</p>
				
				<p>
					L'originalité de la <accronym title="Théorie Relative de la Monnaie">TRM</accronym> est d'inventer un type de monnaie qui répond à cette fonction de la monnaie, d'être <strong>une réelle unité de mesure, au sens où la physique l'entend</strong>.
				</p>
				
				<p>
					En physique, définir une unité de mesure passe par la recherche d'un <strong>invariant</strong>.
				</p>
					
				<p>
					Mais quel est l'invariant d'une économie ?
				</p>
				
				<p>
					Pour le découvrir, regardez la conférence donnée par Stéphane Laborde en 2014 devant le public du <accronym title="Mouvement Français pour un Revenu de Base">MFRdB</accronym>.
				</p>
			</figcaption>
			
			<iframe width="560" height="315" src="https://www.youtube.com/embed/PdSEpQ8ZtY4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
		</figure>
	</div>
</section>


<section id="liberent">
	<div>
		<h2>
			Des monnaies qui libèrent
		</h2>
		
		<figure>
			<blockquote>
					Ceux qui ont quitté ce monde et ceux qui n’existent pas encore sont à la plus grande distance les uns des autres que l’imagination humaine puisse concevoir : quelle possibilité d’obligation peut-il donc y avoir entre eux ? Quelle règle ou quel principe peut-on poser pour que deux êtres imaginaires dont l’un a cessé d’être et l’autre n’existe pas encore, et qui ne peuvent jamais se rencontrer dans ce monde, l’un soit autorisé à maîtriser l’autre jusqu’à la consommation des siècles ?
					
					<cite>« Les droits de l’homme », 1791, Thomas Paine</cite>
			</blockquote>
			
			<figcaption>
				<p>
					Qui possède de la monnaie a une créance sur l'économie&nbsp;: il est en droit d'exiger des autres des valeurs économiques 
					en échange de ses signes monétaires.
				</p>
				<p>
					Qui ne possède pas ou peu d'unités d'une certaine monnaie, mais se voit intimé d'en avoir, que ce soit l'impôt (pouvoir libératoire) 
					ou, par voie de conséquence, parce que ladite monnaie est un quasi-monopole de fait au sein de la zone économique 
					dans laquelle l'individu évolue, devra obéir à ceux qui détiennent des unités monnaie. 
					Il devra travailler pour eux.
				</p>
				<p>
					Posséder la monnaie, c'est donc avoir le pouvoir de commander aux autres.
				</p>
				<p>
					Mais pourquoi le premier aurait-il le droit de commander au second, si le second n'a jamais commandé au premier ?
				</p>
				<p>
					Où est la réciprocité ?
				</p>
				<p>
					Au nom de quoi le second aurait-il une dette de travail envers le premier, s'il n'a jamais échangé avec lui ?
				</p>
				
				<!--
				<p>
					C'est là toute l'inéquité de l'asymétrie temporelle propre aux monnaies dettes&nbsp: si des morts se sont endettés en transmettant de la monnaie, c'est aux nouveaux-nés de rembourser la dette qu'ils ont contracté, en travaillant pour récupérer un peu de monnaie.
				</p>
				-->
			</figcaption>
		</figure>
	</div>
</section>



<section id="monnaies-convergentes">
	<div>
		<h2>
			Une monnaie convergente
		</h2>

		<figure>
			<figcaption>
				<p>
					Une des propriétés d'une monnaie libre est que les soldes de tout un chacun tendent à converger vers la moyenne, à échelle d'une demi vie.
				</p>
				<p>
					Ci-contre, un graphique représentant les soldes en monnaie libre de deux individus fictifs vivant dans un espace monétaire comme la France, où l'espérance de vie est de 80 ans&nbsp;: s'il n'effectuent aucun échange, les deux individus voient leurs soldes converger vers la moyenne à échelle de la moitié de leur espérance de vie, c'est-à-dire 40 ans.
				</p>
				
				<p>
					Cette convergence vers la moyenne correspond à l'annulation des créances sur les morts&nbsp;: 
					les créances des vivants s'annulent au fur et à mesure que s'éteignent les êtres humains sur 
					lesquels les possesseurs de monnaie avaient une créance.
				</p>
				
				<p>
					Puisque les morts ne peuvent plus travailler pour produire les valeurs contre lesquels devrait 
					s'échanger cette monnaie créancière, il est logique que leur dette s'annule, et ne soit pas héritée 
					par les nouveaux nés, dont la plupart ne sont même pas leurs enfants.
				</p>
				
				<p>
					Car il n'est pas nécessaire d'être à découvert pour être techniquement "endetté" ; il suffit de posséder moins que la 
					ce que possède, en moyenne, chaque humain participant à la zone monétaire.
				</p>
			</figcaption>

			<img src="{attach}/images/monnaies-libres/convergence-des-soldes.png" />

		</figure>
	</div>
</section>

			


<section id="symetrie-spatiale">
	<div>
		<h2>
			Pour chaque individu, un égal accès à la monnaie
		</h2>
		
		<figure>
			<img src="{attach}/images/monnaies-libres/symetrie-spatiale.png" />
			
			<figcaption>
				<p>
					Grâce au dividende universel, chaque individu a accès à la même part de monnaie que chacun de ses contemporains.
				</p>
				
				<p>
					Dans la <accronym title="Théorie Relative de la Monnaie">TRM</accronym>, cela s'appelle la "symétrie spatiale".
				</p>
			</figcaption>
		</figure>
	</div>
</section>



<section id="symetrie-temporelle">
	<div>
		<h2>
			Pour chaque génération, un égal accès à la monnaie
		</h2>
		
		<figure>
			<img src="{attach}/images/monnaies-libres/symetrie-temporelle.png" />
			
			<figcaption>
				<p>
					Pour que toutes les générations aient accès à la même part de monnaie, la masse monétaire croît proportionnellement à l'espérance de vie.
				</p>
				
				<p>
					Dans la <accronym title="Théorie Relative de la Monnaie">TRM</accronym>, cela s'appelle la "symétrie temporelle".
				</p>
					
				</p>
			</figcaption>
		</figure>
	</div>
</section>



<section id="M">
	<div>
		<h2>
			Une masse monétaire prévisible
		</h2>
		
		<figure>
			<img src="{attach}/images/monnaies-libres/M.png" />
			
			<figcaption>
				<p>
					En monnaie libre, la masse monétaire future est prévisible, puisqu'elle est définie par des équations mathématiques, et non décidée arbitrairement au gré des politiques monétaires des banques centrales, des taux directeurs qu'elles définissent, et de l'audace ou de la frilosité des banques privées à prêter.
				</p>
			</figcaption>
		</figure>
	</div>
</section>




<section id="DU">
	<div>
		<h2>
			Un dividende universel bien défini
		</h2>
		
		<figure>
			<img src="{attach}/images/monnaies-libres/DU.png" />
			
			<figcaption>
				<p>
					En monnaie libre, la création monétaire se fait par tous, à travers un dividende universel.
				</p>
				<p>
					Comme la masse monétaire, le montant de ce dividende universel est prévisible.
				</p>
				
				<p>
					Prévisible à tel point qu'il sert d'unité de mesure des échanges. 
					Les utilisateurs d'une monnaie libre expriment souvent leurs prix "en DU".
				</p>
			</figcaption>
		</figure>
	</div>
</section>








<section id="sans-crise">
	<div>
		<h2>
			Une monnaie à l'épreuve des crises monétaires
		</h2>
		
		<figure id="hyperinflation">
			<figcaption>
				<h3>Pas de risque d'hyperinflation</h3>
				
				<p>
					Parce que la quantité de monnaie qu'il y aura demain dans l'économie est prévisible, 
					il n'y a jamais de crise de rattrapage, c'est-à-dire qu'il n'y a jamais d'hyperinflation brutale.
				</p>

			</figcaption>
			
			<figure>
				<img src="{attach}/images/monnaies-libres/geekscottes_117.png" />
				
				<figcaption class="attribution">
					D'après un dessin original de Johann "nojhan" Dréo, 2008-10-14, licence GNU FDL
				</figcaption>
			</figure>
		</figure>

		<figure id="deflation">
			<figcaption>
				<h3>Pas de déflation possible </h3>
				
				<p>
					En monnaie libre, la création monétaire ne s'arrête jamais, et tout le monde en bénéficie.
				</p>
				<p>
					Pour ces deux raisons, il est impossible qu'une petit part de la population confisque l'outil d'échange en le thésaurisant excessivement.
				</p>
				<p>
					La monnaie reste donc abondante dans l'économie, et cette dernière n'est alors jamais exsangue ni gelée.
				</p>

			</figcaption>
			
			<!-- image de dragon sur son tas d'or -->
		</figure>
	</div>
</section>




<section id="code-ouvert">
	<div>
		<h2>
			Une monnaie à code ouvert
		</h2>
		
		<figure>
			<figcaption>
				<p>
					Contrairement aux monnaies dettes, régies par les accords de Bâle, ensemble de règles monétaires comparables à un code propriétaire, le code monétaire d'une monnaie libre est modifiable démocratiquement par ses utilisateurs.
				</p>
				
			</figcaption>
			
			<div>
				<div style="padding:56.25% 0 0 0;position:relative;">
					<iframe src="https://player.vimeo.com/video/16999277" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
				</div>
				<script src="https://player.vimeo.com/api/player.js"></script>
				
			</div>
		</figure>
	</div>
</section>



<section id="quatre-libertes">
	<div>
		<h2>
			Les 4 libertés d'une monnaie libre
		</h2>
		
		<figure>
			<figcaption>
				<p>
					De la même façon qu'un logiciel libre est plus que simplement <em>open source</em>, 
					une monnaie libre n'est pas seulement une monnaie dont on peut lire le code monétaire.
				</p>
				<p>
					Une monnaie ne peut être dite <q>libre</q> qu'à condition de respecter les 4 libertés suivantes&nbsp;:
				</p>
				<ol>
					<li>La liberté de choix de son système monétaire</li>
					<li>La liberté d’utiliser les ressources</li>
					<li>La liberté d’estimation et de production de toute valeur économique</li>
					<li>La liberté d’échanger, comptabiliser afficher ses prix “dans la monnaie”</li>
				</ol>
			</figcaption>
			
			<figure>
				<img src="{attach}/images/monnaies-libres/geekscottes_038.png" />
				
				<figcaption class="attribution">
					D'après un dessin original de Johann "nojhan" Dréo, 2007-02-27, Licence Creative Commons BY-SA 2.5
				</figcaption>
			</figure>
		</figure>
	</div>
</section>

<!--
<section id="convergence-des-pensees">
	<div>
		<h2>
			Tous les chemins mènent à la <acronym title="Théorie Relative de la Monnaie">TRM</acronym>
		</h2>
		
		<p>
			Copier/coller/retravailler le contenu de&nbsp;:<br />
			
			<a href="https://web.archive.org/web/20160323220908/http://wiki.creationmonetaire.info/index.php?title=Yoland_Bresson#Liens_avec_d.27autres_chercheurs">
				http://wiki.creationmonetaire.info/index.php?title=Yoland_Bresson#Liens_avec_d.27autres_chercheurs
			</a>
		</p>
	</div>
</section>
-->

## Aller plus loin

* [Théorie Relative de la Monnaie](http://trm.creationmonetaire.info/), par Stéphane Laborde
* Approfondir la TRM :
    * [Le module Galilée](http://rml.creationmonetaire.info/modules/index.html)
    * [Le module Yoland Bresson](http://rml.creationmonetaire.info/modules/module_yoland_bresson.html)
    * [Le module Leibnitz](http://rml.creationmonetaire.info/modules/module_leibnitz.html)
* [Ğlibre](http://www.glibre.org/)

### Compréhension et vulgarisation

* [Questions fréquentes sur les monnaies libres]({filename}monnaies-libres/faq-ml.md)
* *[La TRM En Couleur](http://cuckooland.free.fr/LaTrmEnCouleur.html)* par cuckooland
* *[La TRM pour les enfants](http://cuckooland.free.fr/LaTrmPourLesEnfants.html)* par cuckooland
* *[La TRM en détails](http://monnaie.ploc.be/)* par Emmanuel Bultot
* Vidéo par [Vincent Texier aux RML7](https://www.youtube.com/watch?v=pSaPjxIpJGA&list=PLr7acQJbh5rzgkXOrCws2bELR8TNRIuv0&index=9) abordant les raisons d'une monnaie libre, à Laval en juin 2015
* Vidéo de [Stéphane Laborde à l'Université d'été du MFRB](https://www.youtube.com/watch?v=PdSEpQ8ZtY4) sur la monnaie libre, en été 2014
* Vidéo *[Le paradigme TRM et le RdB](https://www.youtube.com/watch?v=PdSEpQ8ZtY4)* par Stéphane Laborde
* Vidéo *[Révolution monétaire : débat entre Etienne Chouard, Stéphane Laborde et Jean-Baptiste Bersac](https://www.youtube.com/watch?v=kvjstlFaxUw)* par Le 4ème singe
* Vidéos *[Discussions sur la monnaie libre (Étienne Chouard / Stéphane Laborde)](https://www.youtube.com/playlist?list=PL9isKtkLy16dtuwbYGW_KXP99WRCo85pQ)* montées par Denis La Plume
* Vidéo de [Stéphane Laborde à l'Ubuntu Party](https://www.youtube.com/watch?v=ljflI-JAsbc) sur la monnaie libre, à Paris en novembre 2014
* *[« Monnaie de singe et Monnaie Libre » sur RADIOM ](https://www.radiom.fr/podcast/71-passeurs-de-bougnettes/3453-monnaie-de-singe-et-monnaie-libre.html)* par matiou et nadou
* *[« La Monnaie Libre au coeur des débats » sur RADIOM ](https://www.radiom.fr/podcast/71-passeurs-de-bougnettes/3621-la-monnaie-libre-au-coeur-des-debats.html)* par matiou et nadou
* *[Le livre « La monnaie : ce qu'on ignore » de Denis La Plume](https://blog.denislaplume.fr/2017/09/29/le-livre-la-monnaie-ce-quon-ignore-est-sorti/)*

### Articles 

* *[De l'intérêt d'une monnaie libre !](https://blog.cgeek.fr/de-linteret-dune-monnaie-libre.html)* par cgeek
* *[Duniter, Théorie relative de la monnaie et projets autour des monnaies libres](https://moul.re/blog/index.php?article3/duniter)* par Moul

### Blockchain

* [La blockchain : une solution technique pour la monnaie libre]({filename}blockchain.md) par cgeek
* [Comment fonctionne Duniter]({filename}fonctionnement.md) par cgeek
* [Le projet OpenUDC](https://github.com/Open-UDC/open-udc), précurseur de Duniter
* Vidéo *[Une monnaie libre avec OpenUDC/uCoin](https://www.youtube.com/watch?v=ljflI-JAsbc)* par Stéphane Laborde
