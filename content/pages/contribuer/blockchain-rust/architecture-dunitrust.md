Title: Architecture de Dunitrust
Order: 10
Date: 2018-03-29
Slug: architecture-dunitrust
Authors: elois

# Architecture de Dunitrust

Dunitrust est composé de 3 types de crates : 

* Les bibliothèques  
* Les plugins  
* la crate duniter-core

## Arbre des dépendances

::uml::

@startuml

title Arbre des dependances

package "Libraries" {
    package "Level 0" {
        [crypto]
        [wotb]
    }
    package "Level 1" {
        [crypto] ---> [documents]
    }
    package "Level 2" {
        [crypto] ---> [module]
        [documents] ---> [module]
    }
    package "Level 3" {
        [crypto] ---> [conf]
        [module] ---> [conf]
        [crypto] ---> [network]
        [documents] ---> [network]
        [module] ---> [network]
    }
    package "Level 4" {
        [crypto] ---> [dal]
        [documents] ---> [dal]
        [module] ---> [dal]
        [network] ---> [dal]
        [wotb] ---> [dal]
        [crypto] ---> [message]
        [documents] ---> [message]
        [module] ---> [message]
        [network] ---> [message]
        [dal] ---> [message]
    }
}

package "Core" {
    [crypto] ---> [blockchain]
    [wotb] ---> [blockchain]
    [documents] ---> [blockchain]
    [module] ---> [blockchain]
    [conf] ---> [blockchain]
    [network] ---> [blockchain]
    [dal] ---> [blockchain]
    [message] ---> [blockchain]
    [blockchain] ---> [core]
    
    [crypto] ---> [core]
    [module] ---> [core]
    [conf] ---> [core]
    [message] ---> [core]
}

package "DefaultPlugins" {
    [crypto] ---> [ws2p]
    [wotb] ---> [ws2p]
    [documents] ---> [ws2p]
    [module] ---> [ws2p]
    [conf] ---> [ws2p]
    [network] ---> [ws2p]
    [dal] ---> [ws2p]
    [message] ---> [ws2p]
    [crypto] ---> [tui]
    [documents] ---> [tui]
    [module] ---> [tui]
    [conf] ---> [tui]
    [network] ---> [tui]
    [dal] ---> [tui]
    [message] ---> [tui]
}

package "OptionalPlugins" {
    package "Dasa" {
        [crypto] ---> [dasa]
        [wotb] ---> [dasa]
        [documents] ---> [dasa]
        [module] ---> [dasa]
        [conf] ---> [dasa]
        [network] ---> [dasa]
        [dal] ---> [dasa]
        [message] ---> [dasa]
    }
}

package "Main" {
    [core] ---> [main]
    [tui] ---> [main]
    [ws2p] ---> [main]
    [dasa] ---> [main]
}

@enduml

::end-uml::
