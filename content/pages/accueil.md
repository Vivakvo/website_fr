Title: Blockchain + dividende universel
Breadcrumb: Accueil
BodyId: home
BodyClass: pages-principales
URL:
save_as: index.html

<header id="showcase">
	<div>
		<figure>
			<img src="{attach}/images/logos/Duniter-logo.svg" />
		</figure>

		<h2 id="USP">
			<span>Moteur de blockchain</span>
			<span>pour la monnaie libre Ğ1</span>
		</h2>
	</div>
</header>

<section id="TRM">
	<div>
		<figure>
			<figcaption>
				<h2>
					<span>Une monnaie libre :</span>
					<span>une monnaie qui libère</span>
				</h2>
				
				<p>
					Duniter est un logiciel développé pour propulser une monnaie libre&nbsp;: la Ğ1.
				</p>
				
				<p>
					La monnaie libre est un concept introduit en 2010 par un mathématicien français du nom de 
					Stéphane Laborde dans son ouvrage intitulé <strong>Théorie Relative de la monnaie</strong> 
					(<accronym title="Théorie Relative de la Monnaie">TRM</accronym>).
				</p>

				<p>
					Une monnaie libre met tous ses membres à égalité devant la création monétaire, car elle garantit que&nbsp:
				</p>

				<ol>
					<li>deux individus créent la même part de monnaie à tout instant t, </li>
					<li>deux individus créeront, au cours de leur vie, la même part de monnaie (et ce même s'ils vivent à deux époques différentes).</li>
				</ol>
				
				<p>
					Cette égalité devant la création monétaire nécessite que la monnaie soit créée uniquement à travers
					un <strong>dividende universel</strong>.
				</p>

				<a class="CTA" href="{filename}monnaies-libres.md">
					Découvrir les monnaies libres
				</a>
			</figcaption>
			
			<img src="{attach}/images/homepage/theorie-relative-de-la-monnaie-stephane-laborde.jpeg" />
		</figure>
	</div>
</section>

<section id="G1">
	<div>
		<figure>
			<figcaption>
				<h2>
					<span>La Ğ1&nbsp;:</span> <span>monnaie libre</span> <span>propulsée par Duniter</span>
				</h2>
				
				<p>
					Depuis mars 2017, Duniter fait fonctionner la première monnaie libre de l'histoire de l'humanité&nbsp;: la Ğ1 (prononcez "Ğ une" ou "jüne").
				</p>

				<p>
					Première expérience grandeur nature d'une monnaie libre, l'aventure embarque chaque jour quelques explorateurs supplémentaires.
				</p>
				
				<a class="CTA" href="{filename}monnaie-libre-g1.md">
					En savoir plus sur la Ğ1
				</a>
			</figcaption>
			
			<img src="{attach}/images/homepage/logo-G1-origin-dig-00-cc-by-sa.svg" />
		</figure>
	</div>
</section>

<section id="UBI">
	<div>
		<h2>
			<span>Le projet</span> <span>de dividende universel</span> <span>le plus abouti</span>
		</h2>
		
		<figure>
			<figcaption>

				<p>
					De nombreuses initiatives essayent de combiner blockchain et une forme ou une autre de "revenu de base"&nbsp;: 
					Circles, Value Instrument, Good Dollar, Swift Demand, brightID, UBIC, trustlines, Greshm...
				</p>

				<p>
					Fin janvier 2019, un membre s'est rendu à Berlin
					à l'occasion de l'<accronym title="Open Universal Basic Income">OpenUBI</accronym> 
					<a href="https://www.youtube.com/watch?v=7LqdI7pALBE">pour y présenter la Ğ1</a> et s'enquérir des projets similaires actuellement en cours de développement.
				</p>

				<p>
					Sa conclusion&nbsp;: Duniter semble aujourd'hui être le plus abouti d'entre eux.
				</p>

				<p>
					Son compte rendu est consultable ici&nbsp;:
				</p>

				<a class="CTA" href="{attach}/docs/compte-rendu-reunion-Open-UBI.pdf">
					Comparatif des différents projets 
					blockchain + revenu de base (PDF)
				</a>
			</figcaption>
			
			<img src="{attach}/images/homepage/autres-initiatives-blockchain-et-revenu-de-base.png" />
		</figure>
	</div>
</section>






<section id="longevite" class="stats">
	<div>
		<h2>
			Un projet qui tient dans le temps
		</h2>

		<dl class="stats has-3">
			<dt>
				<span>9</span>
				ans
			</dt>
			<dd>
				<p>
					Neuf années d'histoire entre l'écriture de la Théorie Relative de la Monnaie et aujourd'hui.
				</p>
			</dd>
			
			<dt>
				<span>14</span>
				RML
			</dt>
			
			<dd>
				<p>
					Tous les 6 mois depuis juin 2013, sont organisées les <strong>Rencontres Monnaies Libres.</strong>
				</p>

				<p>
					Début juin 2020 aura lieu la 15<sup>ème</sup> édition.
				</p>
			</dd>
			
			<dt>
				<span>22</span>
				développeurs
			</dt>
			
			<dd>
				<p>
					mais aussi de nombreux utilisateurs (les "junistes") qui contribuent, par exemple en organisant des apéros monnaie libre.
				</p>
			</dd>
		</dl>
	</div>
</section>


<section id="toile-de-confiance">
	<div>
		<h2>
			Un système d’identification décentralisé
		</h2>

		<figure>
			<img src="{attach}/images/homepage/toile-de-confiance.png" />

			<figcaption>
				<p>
					Les développeurs de Duniter ont choisi d'utiliser une <strong>Toile de Confiance</strong>, pour 
					permettre aux membres de s'assurer qu'il n'y a pas de faux-monnayeurs dans le système.
				</p>
				<p>
					Ce système d’identification basé sur la <strong>théorie des graphes</strong>
					permet à Duniter de ne pas dépendre d'une entité centrale chargée d'émettre des documents d'identité.
				</p>
				<p>
					L'identification est ici <strong>pair-à-pair</strong>.
				</p>

				<a class="CTA" href="{filename}toile-de-confiance.md">
					Découvrir la Toile de Confiance
				</a>
			</figcaption>
		</figure>
	</div>
</section>


<section id="energivore">
	<div>
		<h2>
			<span>La moins énergivore</span> <span>de toutes les blockchains</span>
		</h2>

		<figure>
			<figcaption>
				<p>
					Dans Duniter, il n'y a pas de récompense particulière à calculer un bloc. 
				</p>
				<p>
					<strong>Pas de course à la puissance de calcul</strong> donc.
				</p>
				<p>
					Dans Duniter, les machines utilisées pour le calcul sont des ordinateurs domestiques, éventuellement des serveurs ou plus simplement des <strong>Raspberry Pi</strong>.
				</p>

				<a class="CTA" href="{filename}duniter-est-il-energivore.md">
					Duniter est-il énergivore ?
				</a>
			</figcaption>
			
			<img src="{attach}/images/homepage/light.svg" />
		</figure>
	</div>
</section>




<section id="implementations">
	<div>
		<h2>
			1 protocole, 3 implémentations
		</h2>
		
		<ol class="logiciels">
			<li>
				<h3>Duniter</h3>

				<img src="{static}/images/logos/duniter-logo.png" />

				<p>
					Première implémentation du protocole en <strong>Typescript</strong> et <strong>Node.js</strong>. Elle expose les API WS2P pour les communications inter-nœuds et BMA pour les clients.
				</p>
			</li>

			<li>
				<h3>Dunitrust</h3>

				<img src="{static}/images/logos/dunitrust.png" />

				<p>
					Implémentation en cours de développement du protocole en <strong>Rust</strong>. Son objectif est de renforcer la sécurité de l'écosystème en cas de faille ou de bug dans Duniter.
				</p>
			</li>

			<li>
				<h3>Juniter</h3>

				<img src="{static}/images/logos/juniter-logo.png" />

				<p>
					Implémentation en cours de développement du protocole en <strong>Java</strong>.
				</p>
			</li>
		</ol>
	</div>
</section>

<section id="clients">
	<div>
		<h2>
			3 clients
		</h2>
		
		<ol class="logiciels">
			<li>
				<h3>Cesium</h3>

				<img src="{static}/images/logos/cesium.png" />

				<p>
					Développé avec les frameworks <strong>AngularJS</strong> et <strong>Ionic</strong>, Cesium est un client multiplateforme.
				</p>
			</li>

			<li>
				<h3>Sakia</h3>

				<img src="{static}/images/logos/sakia.png" />

				<p>
					Développé en <strong>Python</strong> avec la bibliothèque <strong>PyQt5</strong>, Sakia est un client de bureau.
				</p>
			</li>

			<li>
				<h3>Silkaj</h3>

				<img src="{static}/images/logos/silkaj-logo.png" />

				<p>
					Silkaj est un client en ligne de commande développé en <strong>Python</strong>.
				</p>
			</li>

			<li>
				<h3>DuniterPy</h3>

				<img src="{static}/images/logos/duniterpy-logo.png" />

				<p>
					DuniterPy est une bibliothèque <strong>Python</strong>.
				</p>
				
				<p>
					Elle est actuellement utilisée directement par les clients Sakia et Silkaj,
					et indirectement par de nombreux autres logiciels qui se basent sur Silkaj.
				</p>
				
				<p>
					DuniterPy peut être aisément utilisé pour implémenter un client Python.
				</p>
			</li>
		</ol>
	</div>
</section>





<section id="ecosysteme">
	<div>
		<h2>
			Un écosystème logiciel déjà bien fourni
		</h2>
		
		<ul class="logiciels">
			<li id="gchange">
				<h3>Ğchange</h3>

				<p>
					Ğchange est un logiciel de place de marché.
				</p>

				<p>
					Il est implémenté sous la forme d'une interface ionic.js utilisant un serveur Elasticsearch en backend. 
					Comme Césium, il offre des fonctionnalités de création de pages pour les entreprises. 
					Il permet de géolocaliser les différentes annonces.
				</p>

				<p>
					C'est aujourd'hui la place de marché la plus active de la communauté.
				</p>

				<p>
					<a href="https://www.gchange.fr/">
						Visiter Ğchange
					</a>
				</p>

				<p>
					<a href="https://git.duniter.org/search?search=gchange&group_id=165">
						Dépôts GitLab relatifs à Ğchange
					</a>
				</p>
			</li>

			<li id="gannonce">
				<h3>Ğannonce</h3>

				<p>
					Ğannonce est un logiciel de place de marché implémenté sous la forme d'un plugin sur un nœud Duniter.
				</p>

				<p>
					Cette place de marché offre une fonctionnalité intéressante pour financer les différentes initiatives&nbsp;:
					la possibilité de mettre en place un financement participatif ("<em>crowdfunding</em>").
				</p>

				<p>
					Ğannonce cherche sur le portefeuille cible toutes les transactions ayant pour commentaire celui choisi pour le financement participatif, 
					et met automatiquement à jour la jauge de financement en conséquence.
				</p>

				<p>
					<a href="https://gannonce.duniter.org/">
						Visiter Ğannonce
					</a>
				</p>
			</li>

			<li id="barre-de-financement-integrable">
				<h3>La barre de financement intégrable</h3>

				<p>
					Générateur de barre de financement intégrable dans une page web pour suivre l'évolution d'un financement participatif.
				</p>

				<p>
					Permet d'intégrer la barre de progression via une &lt;iframe/&gt; ou une image.
				</p>

				<p>
					<a href="https://git.duniter.org/paidge/barre-de-financement-int-grable">
						Voir le dépôt de la barre de financement intégrable
					</a>
				</p>
				
				<p>
					<a href="https://wotmap.duniter.org/iframe/generate.php">
						Générateur d'image et d'&lt;iframe/&gt; en ligne
					</a>
				</p>
				
			</li>

			<li id="remuniter">
				<h3>Remuniter</h3>

				<p>
					Remuniter est le premier service de <a href="{filename}miner-des-blocs/remuneration-calcul-blocs.md">financement des calculateurs</a>, et le seul connu à présent.
				</p>

				<p>
					Il fonctionne sous la forme d'une caisse commune&nbsp;: n'importe qui peut déposer de la monnaie sur la clé publique associée au service. 
					Il va alors automatiquement verser une petite somme (0,20 Ğ1 par bloc, à l'heure actuelle) aux nœuds qui participent au réseau. 
				</p>

				<p>
					Ce service est actuellement complètement centralisé et fonctionne sur la confiance accordée à celui qui héberge la redistribution de Ğ1. 
					Il est implémenté sous la forme d'un plugin Duniter.
				</p>

				<p>
					<a href="https://remuniter.cgeek.fr/#/">
						Visiter remuniter.cgeek.fr
					</a>
				</p>
			</li>

			<li id="g1cotis">
				<h3>Ğ1Cotis</h3>

				<p>
					Ğ1Cotis est un système de cotisations volontaires en monnaie libre.
				</p>
				<p>
					Ğ1Cotis permet de reverser un pourcentage de transaction(s) à un ou plusieurs comptes, paramétrables selon vos souhaits.
				</p>

				<p>
					<a href="http://g1pourboire.fr/G1cotis.html">
						Visiter g1pourboire.fr/G1cotis
					</a>
				</p>
			</li>

			<li id="g1pourboire">
				<h3>Ğ1Pourboire</h3>

				<p>
					Ğ1Pourboire est un système qui permet d'imprimer des petits papiers qui peuvent êtres 
					utilisés pour laisser un pourboire.
				</p>
				<p>
					Ces petits papiers contiennent des codes d'accès à un portefeuille dédié, depuis lequel 
					le bénéficiaire pourra transférer la monnaie contenue.
				</p>

				<p>
					<a href="http://g1pourboire.fr/">
						Visiter g1pourboire.fr
					</a>
				</p>
			</li>

			<li id="vanitygen">
				<h3>VanityGen</h3>

				<p>
					Programme qui permet de créer une clef publique contenant un certain schéma.
				</p>

				<p>
					Avec VanityGen, vous pouvez par exemple créer une clef publique de la forme&nbsp;:
				</p>
				
				<pre>CedricMoreau000BQCsfyJrAUDZJTqfnQHqJm2E89Vc</pre>

				<p>
					ou encore&nbsp;:
				</p>
					
				<pre>ChaturangaSAS000qcsfyJrAUDZJTqfnQHqJm2E89Vc</pre>

				<p>
					<a href="https://github.com/jytou/vanitygen">
						Visiter le dépôt de VanityGen
					</a>
				</p>
			</li>

			<li id="gsper">
				<h3>Gsper</h3>

				<img src="{static}/images/logos/gsper-logo.svg" />

				<p>
					Programme pour essayer de retrouver un mot de passe perdu, par force brute.
				</p>

				<p>
					<a href="https://gsper.duniter.io/">
						Visiter gsper.duniter.io
					</a>
				</p>

				<p>
					Voir aussi&nbsp;: <br />
					<a href="https://forum.monnaie-libre.fr/t/rml14-erreur-de-transaction-portefeuille-fantome/8573/34">
						L'aternative de Matograine, en Python
					</a>
				</p>
			</li>

			<li id="g1sms">
				<h3>Ḡ1sms</h3>

				<p>
					Un  système de paiement par SMS.
				</p>

				<p>
					<a href="https://www.g1sms.fr/">
						Visiter g1sms.fr
					</a>
				</p>
			</li>

			<li id="g1billet">
				<h3>Ğ1Billet</h3>

				<p>
					Pour imprimer votre propre monnaie, avec des <strong>QR codes</strong> et des billets à gratter.
				</p>

				<p>
					<a href="https://www.g1sms.fr/fr/blog/g1billet">
						Visiter g1sms.fr/g1billet
					</a>
				</p>
			</li>

			<li id="g1tag">
				<h3>Ğ1Tag</h3>

				<p>
					Capsule IPFS chiffrée qui conserve le montant en centimes de Ḡ1 (ZEN) dépensés à sa création
				</p>

				<p>
					<a href="https://www.g1sms.fr/fr/blog/g1tag">
						Visiter g1sms.fr/g1tag
					</a>
				</p>
			</li>

			<li id="wotwizard">
				<h3>WOT Wizard</h3>

				<img src="{static}/images/logos/wot-wizard-black-and-white.png" />

				<p>
					Développé en <strong>Go</strong>, WOT Wizard permet de calculer les probabilités futures d’entrées d’une identité dans la toile de confiance
				</p>

				<p>
					<a href="https://wot-wizard.duniter.org/">
						Visiter wot-wizard.duniter.org
					</a>
				</p>
			</li>

			<li id="wotmap">
				<h3>WOT map</h3>

				<img src="{static}/images/logos/wotmap.jpg" />

				<p>
					Logiciel de visualisation de la toile de confiance sous forme d'un graphe.
				</p>

				<p>
					<a href="https://wotmap.duniter.org/">
						Voir la WOT map
					</a>
				</p>
			</li>

			<li id="g1monit">
				<h3>g1-monit</h3>

				<p>
					Logiciel permettant de générer diverses statistiques sur la monnaie libre Ğ1.
				</p>

				<p>
					<a href="https://monit.g1.nordstrom.duniter.org/">
						Visiter une instance g1-monit
					</a>
				</p>

				<p>
					<a href="https://git.duniter.org/nodes/typescript/modules/duniter-currency-monit">
						Dépôt Git du projet
					</a>
				</p>
			</li>

			<li id="gmix">
				<h3>Ğmixer</h3>

				<p>
					Anonymiseur de portefeuille Ğ1.
				</p>
				
				<p>
					<a href="https://zettascript.org/projects/gmixer/">
						Site officiel
					</a>
				</p>
				
				<p>
					<a href="https://git.duniter.org/tuxmain/gmixer-webclient">
						Dépôt du client Javascript
					</a>
				</p>
				
				<p>
					<a href="https://git.duniter.org/tuxmain/gmixer-py">
						Dépôt du serveur
					</a>
				</p>
				
				<p>
					<a href="https://forum.monnaie-libre.fr/t/gmix-anonymiseur-de-porte-feuille-g1/862">
						Sessions de mix
					</a>
				</p>
			</li>
		</ul>
	</div>
</section>


