Title: Indemnisation des développeurs (déc 2018 – fév 2019)
Date: 2018-11-30 22:33
Category: Financement participatif
Thumbnail: images/coins.svg
Author: Moul
Tags: rémunération
Summary: Un billet pour faire le point sur les rémunérations des développeurs sur le cycle décembre 2018, janvier et février 2019.

Voici les détails de rémunérations et paiements pour le prochain cycle de trois mois qui ont été décidés collectivement :

## Rémunération

* Valeur : 20 DU Ğ1

## Contributeurs rémunérés

### Nœuds

* @cgeek : développeur Duniter, mainteneur de la Ğ1, adminsys serveurs, Remuniter, Ğannonce
* @jytou : releases ARM de Duniter, tests de charge tx sur Ğ1-test avec [tgen](https://forum.duniter.org/t/transaction-generator-generateur-de-transactions/4769)
* @elois : développeur Durs, monit
* @junidev : [Juniter](https://github.com/Bertrandbenj/juniter) – implémentation du protocole Duniter en Java.

### Clients

* @kimamila : développeur Césium, Césium +, Ğchange
* @Inso : développeur Sakia et DuniterPy
* @vit : développeur DuniterPy et nouvelle API cliente GVA
* @Moul : développeur Silkaj, adminsys serveurs : sites web, serveur Prosody, forums, mainteneur duniter_ynh
* @Cebash : développeur Silkaj
* @gerard94 : développeur WotWizard, implementation en Go

### Wot

* @Paidge : développeur WotMap
* @tuxmain : développeur de [WorldWotMap](https://zettascript.org/tux/g1/worldwotmap.html)

### Autres

* @1000i100 : développeur Gvu, Gsper, Dunikey, g1lien, Tipăy, autŎtip, adminsys GitLab
* @HugoTrentesaux : fusion des dépôts des sites web, utilisation système d’internationalisation de Pelican
* @thomasbromehead : traductions anglaises des articles du site web, développeur [g1lit](https://github.com/thomasbromehead/g1lit/)
