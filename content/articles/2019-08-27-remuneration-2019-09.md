Title: Indemnisation des développeurs (sept − nov 2019)
Date: 2019-08-27 22:03
Category: Financement participatif
Thumbnail: images/coins.svg
Author: Moul
Tags: rémunération
Summary: Un billet pour faire le point sur les rémunérations des développeurs sur le cycle septembre-octobre-novembre 2019.

## Changements depuis le précédent cycle

* Ajout de Vivakvo, Boris_Paing, Thatoo, Matograine et jfocher
* Retrait de Jytou

## Rémunération

* Valeur : 20 DU Ğ1

## Contributeurs rémunérés

### Nœuds

#### Duniter

* @cgeek : développeur Duniter, mainteneur de la Ğ1, adminsys serveurs, Remuniter, Ğannonce, monit

#### Dunitrust

* @elois : développeur Dunitrust
* @ji_emme : développeur Dunitrust
* @jsprenger : développeur Dunitrust
* @HugoTrentesaux : développeur Dunitrust, fusion des dépôts des sites web avec le système d’internationalisation de Pelican

#### Juniter

* @junidev : [Juniter](https://github.com/Bertrandbenj/juniter)

### Clients

#### Césium

* @kimamila : développeur Césium, Césium +, Ğchange
* @bpresles : développeur Césium et Duniter
* @Boris_Paing : UX Cesium
* @Vivakvo : traduction de Césium et la licence Ğ1 en espéranto
* @Thatoo : mainteneur application Césium pour YunoHost

#### Python

* @Moul : développeur Silkaj, DuniterPy, Duniter, paquets YunoHost, AdminSys
* @matograine : développeur Silkaj et [Ğ1cotis](https://git.duniter.org/matograine/g1-cotis)
* @vit : développeur DuniterPy et nouvelle API cliente GVA
* @jonas: Debian developer, Silkaj packaging in Debian

#### iOS

* @jfoucher : développeur [Gios](https://github.com/jfoucher/gios) client iOS

### Outils

* @tuxmain : développeur de ĞMixer, [WorldWotMap](https://zettascript.org/tux/g1/worldwotmap.html)
* @1000i100 : développeur Gvu, Gsper, Dunikey, g1lien, Tipăy, autŎtip, adminsys GitLab

### WoT

* @gerard94 : développeur WotWizard, implémentation en Go
* @Paidge : développeur WotMap

### AdminSys

* @Inso : adminsys, développeur Sakia et DuniterPy
* @poka : adminsys

## Dons − Financements

[À ce rythme le projet a une visibilité sur deux mois](https://moul.re/cesium/#/app/wot/78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8/stats).

Le projet est en réflexion pour recevoir plus de fonds pour les futurs développements.

Sujet à développer.