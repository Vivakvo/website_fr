# Prévu

## 2019 / Semaine 51

- URLs : 

	- Lister toutes les redirections à mettre en place dans Nginx

- CSS : 

	- corriger overflow #ecosysteme #vanitygen <code></code> (regarder la page financements)
	
## Plus tard

- Jeter un oeil [aux plugins suivants](https://github.com/getpelican/pelican-plugins) :

	- [interlinks](https://github.com/getpelican/pelican-plugins/tree/master/interlinks)
	- [dead links](https://github.com/silentlamb/pelican-deadlinks/tree/8b49b594591ded55ba20b74c54c12a566fe8e9cc)
	
- A verifier :
	
	- Duniter : doit-on parler de NodeJS ou de Typescript ?
	- Demander confirmation que interfaces-specifiques-de-pair.md et clefs-partagees.md vont bien sous Contribuer et non sous Forger
	- Vérifier que la génération de l'UML dans chapitres 5, 6 et 7 du tutoriel fonctionne bien sur le serveur distant
	- Vérifier que TipueSearch fonctionne

- Contenu : 
	
	- Images :
		- Trouver un moyen de faire une image de dragon sur de l'or
		- Ajouter des personnes aux cheveux longs dans la toile de confiance
		- Dans la page "La Toile de confiance en détail", corriger la formule erronée qui est incrustée en haut d'un graphique
		- Envisager d'arrêter d'utiliser les {attach} (ils créent des doublons)
		- Ajouter une jolie image de la toile de confiance : 
			https://zettascript.org/tux/g1/worldwotmap.html
		
	- Textes : 
		- Ajouter "Méthode n°3 : en forger" à la page obtenir-des-g1.md
		- Ajouter Juniter dans la liste des implémentations (?)
		- Ajouter la liste des développeurs sur la page "Qui sommes-nous ?"
		- Dans la partie écosystème de la page d'accueil, ajouter Gvu : http://gvu.g1.1000i100.fr
		- Page G1 : histoires, particularités de cette ML (utilisation d'une toile de confiance, espérance de vie à 80 ans)
		- Ajouter les dons à Remuniter dans le rond fushia
		- Ajouter les Summary à chaque article de blog
		- Créer une page à destination des traducteurs
		- Mettre à jour la page contribuer/duniter/git-branches (mention de GitHub)
		- Mettre à jour la page contribuer/duniter/les-modules-c-cpp (mention de GitHub)
		- Faut-il parler de "Forgeron" ou de "Noeud calculateur" ? (pages "La G1", "Forger des blocs")
		- Comparatif : 

			- vérifier ce qu'est exactement brightID et l'enlever de la liste le cas échéant
			- penser à ajouter Mannabase à la liste
			- faire pointer le bouton vers une page plutôt que vers un PDF
	
- Problèmes CSS : 
	
	- Améliorer style des boutons Télécharger sur la page forger-des-blocs.md

- Conception :

	- Corriger le doublon sur id="content"
	- Remplacer les images incluses via les /images/icons/ par du CSS (warning, avertissement, etc.)
	- Copier le code de paidge pour virer le titre
	- Langues : essayer de finir gestion des liens interlangues amorcée dans fichier `includes/footer.html`
	- Créer fichier de traduction .po pour traduire tout ce qui est ajouté via _()
	- Supprimer les <center></center>
	- Ter-je les fichiers bootstrap (CSS et JS) dès que plus rien en provenant n'est utilisé

- (? toujours d'actualité ?) Résoudre le problème des images .png ou .svg contenues dans /pages/, dont l'extension disparaît à la génération dans /output/

- LazyLoad : précharger les images un peu plus tôt
	
- Acquisition : 

    - Se faire référencer sur https://github.com/getpelican/pelican/wiki/Powered-by-Pelican

- Remettre "Propulsé par Pelican"

- Chercher \]\(\{(static|attach)\}/?images/wiki pour trouver les contenus qui sont encore liés via une arborescence obsolète (et qu'en faire ? les déplacer risque d'être pourri niveau SEO)

- Investiguer le fonctionnement des retours à la ligne simples dans Markdown
