Title: Wiki
Order: 3
Date: 2017-03-27
Tags: wiki, blockchain, trm, yunohost
Slug: wiki
Authors: cgeek, matiou

Cette page regroupe des informations importantes concernant Duniter à des fins d'indexation. 

Elle sert également de base de connaissances pour ses contributeurs.

## Monnaie

* [Licence Ğ1]({filename}monnaie-libre-g1/licence-g1.md) (devenir membre Ğ1 + règles de la monnaie + règles de la Toile de Confiance)

## Plateformes d’échange

Exemple de plateformes d'échange compatible avec les crypto-monnaies Duniter comme la Ğ1 (connexion blockchain)

* [ğannonce](https://gannonce.duniter.org/), site web

Simple groupe d'annonces sur le réseau privateur Facebook

* [Ḡcoin, annonces en monnaie libre](https://www.facebook.com/groups/217329018743538/)

Dépôt media regroupant les logos, banières et autres outils de communication visuels :

* [Communication G1](https://git.duniter.org/communication/G1)
* [Logos Logiciels](https://git.duniter.org/communication/logos)


## Écosystème logiciel

### Logiciel Duniter

Cœur du réseau Duniter, application P2P qui synchronise et modifie la blockchain commune.

## Fondations théoriques

* [Le projet OpenUDC](https://github.com/Open-UDC/open-udc), précurseur de Duniter

