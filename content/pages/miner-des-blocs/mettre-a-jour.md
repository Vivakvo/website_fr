Title: Mettre à jour un nœud Duniter
Order: 9
Date: 2017-06-19
Slug: mettre-a-jour
Authors: cgeek

# Mettre à jour un nœud Duniter

Mettre à jour son nœud Duniter consiste simplement à : 

* [réinstaller son nœud Duniter]({filename}installer.md)
* relancer son nœud pour appliquer les modifications
