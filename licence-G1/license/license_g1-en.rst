License Ğ1 - v0.2.9
===================

:date: 2017-08-21 16:59
:modified: 2019-08-30 19:20

**Money licensing and liability commitment.**

Any certification operation of a new member of Ğ1 must first be accompanied by the transmission of this license of the currency Ğ1. The certifier must ensure that the license has been studied, understood and accepted by the person who will be certified.

Any meeting event about Ḡ1 should include the acknowledgment of this license, which can be read out loud, or given by any means.


Ğ1 Web of Trust (Ğ1 WoT)
------------------------

**Warning:** Certifying a new member of the WoT is more than simply meeting the person to be certified. When certifying someone, you assure the Ğ1 community that you know this person well enough to be able to contact him anytime, and may detect duplicate accounts or other problems (disappearance, etc.).

**Highly recommended advices**

Knowing a person well implies that you are able to contact her by many different means (physically, digitally, other...) but also
that you know some other people who know her as well and can contact her as easily. Notably, if you don't know well the other certifiers it's a strong sign that you don't know the person well enough. A certification made in this context will alert all the Ğ1 community.
In a case of insufficient knowledge, you should not certify.

Never certify alone, but always with another member of the Web of Trust to avoid any software misuse. In case of any error,
immediately contact other community member of the Ğ1 WoT.

Verify that the person to be certified masters her account operations : a good way to verify it is to transfer some Ğ1 to the target account, and to ask then a refund to your own account. This operation will assure you that the certified person masters its private key.

Verify that your contacts have studied and understood an up-to-date version of the license.

If you are aware that an effective certifier or a future certifier of an account does not know the account owner, please alert immediately some
experts of the subject among your contacts in the Ğ1 WoT, for the procedure to be verified by the Ğ1 WoT members.


When you are a member of Ğ1 and you are about to certify a new account identified by a public key:

**You have to make sure:**

1°) To know the individual who manages the public key well enough (not only an acquaintance). Please read the above advices about knowing "well enough".

2°) To personally check with the person to be certified that the public key is correct.

3 °) To have verified with the person concerned that he has indeed generated his Duniter account revocation document, which enables the deactivation of the member account if necessary. (in case of account theft, ID change, incorrectly created account, loss of password, etc.).

4a°) To meet the individual to be certified in person, to check his identity.
 
4b°) Or to Remotely verify the public person / key link by contacting the person via several different means of communication, such as social network + forum + mail + video conference + phone (acknowledge voice).

Indeed it is possible to hack an email account or a forum account, but it is much harder to hack four distinct means of communication, and mimic the appearance (video) as well as the voice of the person.

However, the 4a° is preferable to 4b°, whereas the 1°, 2° and 3° are always required in all cases.

**Brief WoT rules:**

Each member has a stock of 100 possible certifications, which can only be issued at the rate of 1 certification / 5 days.

A certification for a new member is valid for 2 months, and will be permanently accepted if the certified account receives at least 4 other available certifications during these 2 months, otherwise the certification process must be started over.

To become a new member of the Ğ1 WoT, one must be certified 5 times and be at a distance < 5 of 80% of the WoT sentinels.

A member of the Ğ1 WoT is a sentinel when he has received and issued at least Y [N] certifications where N is the number of members of the WoT and Y [N] = ceiling N ^ (1/5). Examples:

* For 1024 < N ≤ 3125 we have Y [N] = 5
* For 7776 < N ≤ 16807 we have Y [N] = 7
* For 59049 < N ≤ 100 000 we have Y [N] = 10

Once the new member is part of the Ğ1 WoT, his certifications remain valid for 2 years.

To remain a member, you must renew your membership regularly with your private key (at least every 12 months) and make sure you have at least 5 valid certifications after 2 years.

The Ğ1 Currency
---------------

Units of Ğ1 are produced by a Universal Dividend (DU) for any human member, which is of the form:

* 1 DU per person per day

The amount of DU is identical each day until the next equinox, where the DU will then be reevaluated according to the formula:

* DU <sub>day</sub> (the following equinox) = DU <day>(equinox) + c² (M / N) (equinox) / (15778800 seconds)</day>

With as parameters:

* c = 4.88% / equinox
* UD (0) = 10.00 Ğ1

And as variables:

* _M_ the total monetary mass at the equinox
* _N_ the number of members at the equinox

Ğ1 Software and Ğ1 license
--------------------------

The Ğ1 software allowing users to manage their use of Ğ1 must transmit this license with the software and all the technical parameters of the Ğ1 currency and Ğ1 WoT which are entered in the initial block 0 of Ğ1.

For more technical details it is possible to read the code of Duniter directly since it is a free software. One can also inspect the data of the Ğ1 blockchain since it is publically available. You can retrieve it via a Duniter instance, another client or by querying directly a Ğ1 node.

More information on the Duniter Team website [https://www.duniter.org](https://www.duniter.org)
