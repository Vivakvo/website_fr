Title: Contribuer à Duniter
BodyClass: pages-principales
Breadcrumb: Contribuer

# Contribuer à Duniter

Vous aimeriez contribuer ?

Vous pouvez [rapporter un bug]({filename}contribuer/rapporter-un-bug.md), 
[améliorer le site]({filename}contribuer/ameliorer-le-site.md), 
le traduire, ou encore participer au développement de 
[l'écosystème Duniter]({filename}contribuer/ecosysteme-logiciel-duniter.md).

Vous avez l'embarras du choix !

<section id="implementations">
	<div>
		<h2>
			Développer le cœur
		</h2>
		
		<ul class="logiciels">
			<li>
				<h3>
					<a href="{filename}contribuer/blockchain-nodejs.md">
						Duniter
					</a>
				</h3>

				<a class="img" 
				   href="{filename}contribuer/blockchain-nodejs.md">
					<img src="{static}/images/logos/duniter-logo.png" />
				</a>

				<p>
					Première implémentation du protocole en <strong>Typescript</strong> et <strong>Node.js</strong>. Elle expose les API WS2P pour les communications inter-nœuds et BMA pour les clients.
				</p>

				<p>
					<a href="{filename}contribuer/blockchain-nodejs.md">
						Tutoriel de développement Duniter
					</a>
				</p>
			</li>

			<li>
				<h3>
					<a href="{filename}contribuer/blockchain-rust.md">
						Dunitrust
					</a>
				</h3>

				<a class="img" 
				   href="{filename}contribuer/blockchain-rust.md">
					<img src="{static}/images/logos/dunitrust.png" />
				</a>

				<p>
					Implémentation en cours de développement du protocole en <strong>Rust</strong>. Son objectif est de renforcer la sécurité de l'écosystème en cas de faille ou de bug dans Duniter.
				</p>

				<p>
					<a href="{filename}contribuer/blockchain-rust.md">
						Tutoriel de développement Dunitrust
					</a>
				</p>
			</li>

			<li>
				<h3>Juniter</h3>

				<img src="{static}/images/logos/juniter-logo.png" />

				<p>
					Implémentation en cours de développement du protocole en <strong>Java</strong>.
				</p>
				
				<p>
					<a href="https://github.com/Bertrandbenj/juniter">
						Dépôt GitHub du projet
					</a>
				</p>
			</li>
		</ul>
	</div>
</section>

<section id="clients">
	<div>
		<h2>
			Développer un client
		</h2>

		<p>
			Contrairement aux logiciels serveurs, les logiciels clients ne font que 
			se connecter au réseau Duniter pour l'utiliser. 
		</p>

		<p>
			Ils ne font pas le travail d'un nœud, mais réalisent des opérations sur la blockchain&nbsp;: 
			créer un porte-monnaie, transférer de la monnaie entre deux porte-monnaies, 
			créer une identité dans la toile de confiance, etc.
		</p>

		<p>
			<a href="{filename}contribuer/preconisations-client-duniter.md">
				Préconisations pour développer un client Duniter
			</a>
		</p>
		
		<ul class="logiciels">
			<li>
				<h3>
					<a href="https://git.duniter.org/clients/cesium-grp/cesium/blob/master/doc/fr/development_tutorial-01.md">
						Cesium
					</a>
				</h3>

				<a class="img" 
				   href="https://git.duniter.org/clients/cesium-grp/cesium/blob/master/doc/fr/development_tutorial-01.md">
					<img src="{static}/images/logos/cesium.png" />
				</a>

				<p>
					Développé avec les frameworks <strong>AngularJS</strong> et <strong>Ionic</strong>, Cesium est un client multiplateforme.
				</p>

				<p>
					<a href="https://git.duniter.org/clients/cesium-grp/cesium/blob/master/doc/fr/development_tutorial-01.md">
						Tutoriel de développement Cesium
					</a>
				</p>
			</li>

			<li>
				<h3>
					<a href="https://git.duniter.org/clients/python/sakia/blob/master/doc/install_for_developers.md">
						Sakia
					</a>
				</h3>

				<a class="img" 
				   href="https://git.duniter.org/clients/python/sakia/blob/master/doc/install_for_developers.md">
					<img src="{static}/images/logos/sakia.png" />
				</a>

				<p>
					Développé en <strong>Python</strong> avec la bibliothèque <strong>PyQt5</strong>, Sakia est un client de bureau.
				</p>

				<p>
					<a href="https://git.duniter.org/clients/python/sakia/blob/master/doc/install_for_developers.md">
						Tutoriel de développement Sakia
					</a>
				</p>
			</li>

			<li>
				<h3>
					<a href="https://git.duniter.org/clients/python/silkaj">
						Silkaj
					</a>
				</h3>

				<a class="img" 
				   href="https://git.duniter.org/clients/python/silkaj">
					<img src="{static}/images/logos/silkaj-logo.png" />
				</a>

				<p>
					Silkaj est un client en ligne de commande développé en <strong>Python</strong>.
				</p>

				<p>
					<a href="https://git.duniter.org/clients/python/silkaj/-/blob/dev/CONTRIBUTING.md">
						Tutoriel de contribution à Silkaj
					</a>
				</p>
			</li>

			<li>
				<h3>DuniterPy</h3>

				<img src="{static}/images/logos/duniterpy-logo.png" />

				<p>
					DuniterPy est une bibliothèque <strong>Python</strong>.
				</p>
				
				<p>
					Elle est actuellement utilisée directement par les clients Sakia et Silkaj,
					et indirectement par de nombreux autres logiciels qui se basent sur Silkaj.
				</p>
				
				<p>
					DuniterPy peut être aisément utilisé pour implémenter un client Python.
				</p>
				<p>
					<a href="https://git.duniter.org/clients/python/duniterpy">
						Dépôt GitLab de DuniterPy
					</a>
				</p>
			</li>
		</ul>
	</div>
</section>



<section id="ecosysteme">
	<div>
		<h2>
			Développer une application tierce
		</h2>
		
		<p>
			Ces applications qui ne sont pas des clients tournent autour du réseau Duniter 
			et apportent des fonctionnalités utiles aux utilisateurs.
		</p>
		
		<ul class="logiciels">
			<li id="gchange">
				<h3>Ğchange</h3>

				<p>
					Ğchange est un logiciel de place de marché.
				</p>

				<p>
					Il est implémenté sous la forme d'une interface ionic.js utilisant un serveur Elasticsearch en backend. 
					Comme Césium, il offre des fonctionnalités de création de pages pour les entreprises. 
					Il permet de géolocaliser les différentes annonces.
				</p>

				<p>
					C'est aujourd'hui la place de marché la plus active de la communauté.
				</p>

				<p>
					<a href="https://www.gchange.fr/">
						Visiter Ğchange
					</a>
				</p>

				<p>
					<a href="https://git.duniter.org/search?search=gchange&group_id=165">
						Dépôts GitLab relatifs à Ğchange
					</a>
				</p>
			</li>

			<li id="gannonce">
				<h3>Ğannonce</h3>

				<p>
					Ğannonce est un logiciel de place de marché implémenté sous la forme d'un plugin sur un nœud Duniter.
				</p>

				<p>
					Cette place de marché offre une fonctionnalité intéressante pour financer les différentes initiatives&nbsp;:
					la possibilité de mettre en place un financement participatif ("<em>crowdfunding</em>").
				</p>

				<p>
					Ğannonce cherche sur le portefeuille cible toutes les transactions ayant pour commentaire celui choisi pour le financement participatif, 
					et met automatiquement à jour la jauge de financement en conséquence.
				</p>

				<p>
					<a href="https://gannonce.duniter.org/">
						Visiter Ğannonce
					</a>
				</p>
			</li>

			<li id="barre-de-financement-integrable">
				<h3>La barre de financement intégrable</h3>

				<p>
					Générateur de barre de financement intégrable dans une page web pour suivre l'évolution d'un financement participatif.
				</p>

				<p>
					Permet d'intégrer la barre de progression via une &lt;iframe/&gt; ou une image.
				</p>

				<p>
					<a href="https://git.duniter.org/paidge/barre-de-financement-int-grable">
						Voir le dépôt de la barre de financement intégrable
					</a>
				</p>
				
				<p>
					<a href="https://wotmap.duniter.org/iframe/generate.php">
						Générateur d'image et d'&lt;iframe/&gt; en ligne
					</a>
				</p>
			</li>

			<li id="remuniter">
				<h3>Remuniter</h3>

				<p>
					Remuniter est le premier service de <a href="{filename}miner-des-blocs/remuneration-calcul-blocs.md">financement des calculateurs</a>, et le seul connu à présent.
				</p>

				<p>
					Il fonctionne sous la forme d'une caisse commune&nbsp;: n'importe qui peut déposer de la monnaie sur la clé publique associée au service. 
					Il va alors automatiquement verser une petite somme (0,20 Ğ1 par bloc, à l'heure actuelle) aux nœuds qui participent au réseau. 
				</p>

				<p>
					Ce service est actuellement complètement centralisé et fonctionne sur la confiance accordée à celui qui héberge la redistribution de Ğ1. 
					Il est implémenté sous la forme d'un plugin Duniter.
				</p>

				<p>
					<a href="https://remuniter.cgeek.fr/#/">
						Visiter remuniter.cgeek.fr
					</a>
				</p>
			</li>

			<li id="g1cotis">
				<h3>Ğ1Cotis</h3>

				<p>
					Ğ1Cotis est un système de cotisations volontaires en monnaie libre.
				</p>
				<p>
					Ğ1Cotis permet de reverser un pourcentage de transaction(s) à un ou plusieurs comptes, paramétrables selon vos souhaits.
				</p>

				<p>
					<a href="http://g1pourboire.fr/G1cotis.html">
						Visiter g1pourboire.fr/G1cotis
					</a>
				</p>
			</li>

			<li id="g1pourboire">
				<h3>Ğ1Pourboire</h3>

				<p>
					Ğ1Pourboire est un système qui permet d'imprimer des petits papiers qui peuvent êtres 
					utilisés pour laisser un pourboire.
				</p>
				<p>
					Ces petits papiers contiennent des codes d'accès à un portefeuille dédié, depuis lequel 
					le bénéficiaire pourra transférer la monnaie contenue.
				</p>

				<p>
					<a href="http://g1pourboire.fr/">
						Visiter g1pourboire.fr
					</a>
				</p>
			</li>

			<li id="vanitygen">
				<h3>VanityGen</h3>

				<p>
					Programme qui permet de créer une clef publique contenant un certain schéma.
				</p>

				<p>
					Avec VanityGen, vous pouvez par exemple créer une clef publique de la forme&nbsp;:
				</p>
				
				<pre>CedricMoreau000BQCsfyJrAUDZJTqfnQHqJm2E89Vc</pre>

				<p>
					ou encore&nbsp;:
				</p>
					
				<pre>ChaturangaSAS000qcsfyJrAUDZJTqfnQHqJm2E89Vc</pre>

				<p>
					<a href="https://github.com/jytou/vanitygen">
						Visiter le dépôt de VanityGen
					</a>
				</p>
			</li>

			<li id="gsper">
				<h3>Gsper</h3>
				
				<img src="{static}/images/logos/gsper-logo.svg" />

				<p>
					Programme pour essayer de retrouver un mot de passe perdu, par force brute.
				</p>

				<p>
					<a href="https://gsper.duniter.io/">
						Visiter gsper.duniter.io
					</a>
				</p>

				<p>
					Voir aussi&nbsp;: <br />
					<a href="https://forum.monnaie-libre.fr/t/rml14-erreur-de-transaction-portefeuille-fantome/8573/34">
						L'aternative de Matograine, en Python
					</a>
				</p>
			</li>

			<li id="g1sms">
				<h3>Ḡ1sms</h3>

				<p>
					Un  système de paiement par SMS.
				</p>

				<p>
					<a href="https://www.g1sms.fr/">
						Visiter g1sms.fr
					</a>
				</p>
			</li>

			<li id="g1billet">
				<h3>Ğ1Billet</h3>

				<p>
					Pour imprimer votre propre monnaie, avec des <strong>QR codes</strong> et des billets à gratter.
				</p>

				<p>
					<a href="https://www.g1sms.fr/fr/blog/g1billet">
						Visiter g1sms.fr/g1billet
					</a>
				</p>
			</li>

			<li id="g1tag">
				<h3>Ğ1Tag</h3>

				<p>
					Capsule IPFS chiffrée qui conserve le montant en centimes de Ḡ1 (ZEN) dépensés à sa création
				</p>

				<p>
					<a href="https://www.g1sms.fr/fr/blog/g1tag">
						Visiter g1sms.fr/g1tag
					</a>
				</p>
			</li>

			<li id="wotwizard">
				<h3>WOT Wizard</h3>

				<img src="{static}/images/logos/wot-wizard-black-and-white.png" />

				<p>
					Développé en <strong>Go</strong>, WOT Wizard permet de calculer les probabilités futures d’entrées d’une identité dans la toile de confiance
				</p>

				<p>
					<a href="https://wot-wizard.duniter.org/">
						Visiter wot-wizard.duniter.org
					</a>
				</p>
			</li>

			<li id="wotmap">
				<h3>WOT map</h3>

				<img src="{static}/images/logos/wotmap.jpg" />

				<p>
					Logiciel de visualisation de la toile de confiance sous forme d'un graphe.
				</p>

				<p>
					<a href="https://wotmap.duniter.org/">
						Voir la WOT map
					</a>
				</p>
			</li>

			<li id="g1monit">
				<h3>g1-monit</h3>

				<p>
					Logiciel permettant de générer diverses statistiques sur la monnaie libre Ğ1.
				</p>

				<p>
					<a href="https://monit.g1.nordstrom.duniter.org/">
						Visiter une instance g1-monit
					</a>
				</p>

				<p>
					<a href="https://git.duniter.org/nodes/typescript/modules/duniter-currency-monit">
						Dépôt Git du projet
					</a>
				</p>
			</li>

			<li id="gmix">
				<h3>Ğmixer</h3>

				<p>
					Anonymiseur de portefeuille Ğ1.
				</p>
				
				<p>
					<a href="https://zettascript.org/projects/gmixer/">
						Site officiel
					</a>
				</p>
				
				<p>
					<a href="https://git.duniter.org/tuxmain/gmixer-webclient">
						Dépôt du client Javascript
					</a>
				</p>
				
				<p>
					<a href="https://git.duniter.org/tuxmain/gmixer-py">
						Dépôt du serveur
					</a>
				</p>
				
				<p>
					<a href="https://forum.monnaie-libre.fr/t/gmix-anonymiseur-de-porte-feuille-g1/862">
						Sessions de mix
					</a>
				</p>
			</li>
		</ul>
	</div>
</section>

## Financement

* [Décisions des affectations des dons aux différents développeurs]({filename}financements.md)
